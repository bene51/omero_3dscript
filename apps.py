from django.apps import AppConfig

class AppNameAppConfig(AppConfig):
	name = "omero_3dscript"
	label = "3Dscript"

